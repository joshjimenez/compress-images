<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('/images');
});

Route::get('/images', 'ImagesController@index');
Route::get('/images/update_directories', 'ImagesController@updateDirectories');
Route::post('/uploadImage', 'ImagesController@uploadImage');
Route::post('/uploadImageAWS', 'ImagesController@uploadImageAWS');
