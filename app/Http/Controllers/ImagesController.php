<?php

namespace App\Http\Controllers;

use App\Directory;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directories = Storage::disk('local');

        return view('pages.images.index', compact('directories'));
    }

    public function uploadImage(Request $request){

        //Validation
        $this->validate($request, [
            "file" => 'required|mimes:jpeg,jpg,png,svg,svgz',
            "directory" => 'required'
        ]);

        //variables
        $compressionpc = 82.2;

        //Get the image
        $image = $request->file('file');
        $directory_path = $request->input('directory');

        $imageName = $image->getClientOriginalName();

        Image::make($image)->save(public_path("gallery/images/".$imageName), $compressionpc);

        /* Comunication with AWS s3 bucket */
          $path = Storage::disk('s3')->putFileAs(
            $directory_path,
            new File(public_path("gallery/images/".$imageName)),
            $imageName,
            [
                   'visibility' => 'public',
                   'CacheControl' => 'max-age=29462400',
            ]
        );

        return $path ; "gallery/images/".$imageName;
        /*return "gallery/images/".$imageName;*/

    }

    public function updateDirectories(){

        $directories = Storage::disk('s3')->allDirectories();

        DB::table('directories')->delete();

        foreach($directories as $directory){
            DB::table('directories')->insert(
                [
                    'driver' => 'AWS s3',
                    'path' => $directory
                ]
            );
        }

        return "done";
    }

}
