<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Images Optimizer</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @yield('styles')
    </head>
    <body>
        {{--includes the navigation--}}
        @include('partials.nav')

        <div class="container">
            {{--Yield the content for every page--}}
            @yield('content')
        </div>

        {{--loads general scripts--}}
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('scripts')
    </body>
</html>
