@extends('welcome')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('css/images.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <h4>Images Index</h4>
        </div>
        <div class="col-xs-6">
            <div class="actions_container">&nbsp;
                <h5><a href="#" id="update_directories">Update Directories</a></h5>
            </div>
        </div>
    </div>
    {{ Form::open(array('url' => '/uploadImage', 'method' => 'post', 'files' => true, 'class' => 'dropzone', 'id' => 'uploadImages')) }}


        {{--AWS s3 connection--}}
        <div class="row">
            <div class="col-xs-12">
                <select name="directory" id="basic_directory_search" class="form-control" style="width: 100%">
                    <option value="">Select a AWS S3 directory</option>
                    @foreach($directories as $directory)
                        <option value="{{ $directory->path }}">{{ $directory->path }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    {{--Dropzone upload images--}}
    {{ Form:: close() }}

    <br>
    <div class="row">
        <div class="col-xs-12">
            <span id="flash_messages"></span>
            <div id="message"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script>
        Dropzone.options.uploadImages = {
            acceptedFiles: "image/*",
            init: function(){
                this.on('sending', function(file, response){
                    $('#flash_messages').append("Loading...");
                });

                this.on('success', function(file, response){
                    $('#flash_messages').empty();

                  $("#message").prepend('<div class="alert alert-success" role="alert">Image Optimized: ' +response+ '</div>');
                  /*$("#message").prepend('<div class="alert alert-success" role="alert">Image Optimized: <a href="' + response + '" download="' + response + '">'+response+'</a></div>');*/
                });

                this.on('error', function(file, response){
                    $('#flash_messages').empty();
                    this.removeFile(file);

                    if(typeof response == 'object'){
                      Object.keys(response).forEach(function(key) {
                          $(response[key]).each(function(index, value){
                              $("#message").prepend('<div class="alert alert-danger" role="alert">'+ value +'</div>');
                          });
                      });

                    }else{
                      $("#message").prepend('<div class="alert alert-danger" role="alert">'+ response +'</div>');
                    }
                });

            }
        }

        $(document).ready(function(){
            $("#basic_directory_search").select2();

            $("#update_directories").click(function(e){
                e.preventDefault();
                $.ajax({
                    url: "/images/update_directories",
                    method: "GET",
                    beforeSend: function(){
                        $('#flash_messages').append("Updating directories... This may take a while.");
                    },
                    success: function(data){
                        bootbox.alert("Directories were properly updated", function(){
                            window.location.reload();
                        });

                    }
                });
            });
        });
    </script>

@endsection